import java.util.Scanner;

public class Fibonacci {
    int f1=0, f2=1, fn, i;
        public Fibonacci(){
            Scanner inputN = new Scanner(System.in);
            System.out.println("input N : ");
            int N = inputN.nextInt();

            for (i = 0; i <= N; i++) {
                if (i == 1) {
                    fn = f1;
                }
                if (i == 2) {
                    fn = f2;
                }
                if (i >= 3) {
                    fn = f2 + f1;
                    f1 = f2;
                    f2 = fn;
                    }
                }
            System.out.println("f(" + N + ")" + "=" + fn);
        }
        @Override
        public String toString() {
            return "";
        }
    
}
